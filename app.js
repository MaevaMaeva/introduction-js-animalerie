
fetch('users.json')
    .then(response => response.json())
    
    //exo1 : 
    //Grâce à l'API Fetch récupérez et affichez le contenu du fichier JSON dans la console (utilisateurs et animaux).
    .then(data => {
        console.log('exo1');
        console.log(data);
        
    //exo2 :
    //N'affichez que la liste des utilisateurs.
        console.log('exo2 : liste des utilisateurs :'); 
        let customs = data['customers'];
        customs.forEach(element=>console.log(element.user_name))
        /* autre façon plus 'facile', plutot qu'avec for comme j'avais fait à l'origine
        for (i=0; i<customs.length; i++){
        console.log(customs[i]['user_name']);
       }*/
    //exo3 :
    // N'affichez que la liste des animaux, classés dans l'ordre alphabétique de leurs noms.
        console.log('exo3 : liste des animaux par ordre croissant :');
        let pets = [];
        for (i=0; i<customs.length; i++){
            customs[i]['user_pets'].forEach(pet=>{pets.push(pet['pet_name']+' the '+pet['pet_type']+', age : '+pet['pet_age']);});
        } // .push() ajoute un élement au tableau
        let petsOrdre = pets.sort(); //.sort() range le tableau par ordre croissant, filter() un peu pareil ? mais créé un nouveau tableau
        for (i=0; i<petsOrdre.length; i++){
        console.log(petsOrdre[i]);
        }
    //exo4 : 
    //N'affichez dans la console que les utilisateurs qui possèdent au moins un animal.
        console.log('exo4 :');
        for (i=0; i<customs.length ; i++){
            if (customs[i]['user_pets'].length>0){
                console.log(customs[i]['user_name']+ ' a au moins un animal.');
            }
        }
    //exo5 :
    //Ajoutez un animal Mickey, de type souris, âgé de 0.1 an à chaque utilisateur puis affichez la liste des utilisateurs.
        console.log('exo5 :');    
        let mickeyMouse = {
            pet_name: "Mickey",
            pet_type: "Mouse",
            pet_age: 0.1
        }
        for (i=0; i<customs.length; i++){
            customs[i]['user_pets'].push(mickeyMouse);
        }
        console.log(customs);
    //exo6:
    //En bonus, parce que le propriétaire adore les chats : 
    //affichez 5 faits au hasard concernant les chats, en vous appuyant sur l'API Cat Facts : 
    //https://alexwohlbruck.github.io/cat-facts/docs/
        console.log('exo6 :');
            fetch('https://cat-fact.herokuapp.com/facts/random?animal_type=cat&amount=5')
            .then(response => response.json())
            .then(data => {
                    data.forEach(data2 =>
                    console.log(data2.text))
             })  
             .catch(error => alert ("Erreur : " + error))
            }) 
    .catch(error => alert ("Erreur : " + error))


